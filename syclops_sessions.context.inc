<?php
/**
 * @file
 * syclops_sessions.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function syclops_sessions_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'member-session';
  $context->description = '';
  $context->tag = 'User';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'session' => 'session',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
        'views-syclops_session-block_2' => array(
          'module' => 'views',
          'delta' => 'syclops_session-block_2',
          'region' => 'content',
          'weight' => '-10',
        ),
        'block-5' => array(
          'module' => 'block',
          'delta' => '5',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-syclops_session-block_4' => array(
          'module' => 'views',
          'delta' => 'syclops_session-block_4',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-syclops_session-block_5' => array(
          'module' => 'views',
          'delta' => 'syclops_session-block_5',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('User');
  $export['member-session'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'project-admin-session';
  $context->description = '';
  $context->tag = 'User';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'session' => 'session',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
        'views-syclops_session-block_2' => array(
          'module' => 'views',
          'delta' => 'syclops_session-block_2',
          'region' => 'content',
          'weight' => '-10',
        ),
        'block-5' => array(
          'module' => 'block',
          'delta' => '5',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-syclops_session-block_4' => array(
          'module' => 'views',
          'delta' => 'syclops_session-block_4',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-syclops_session-block_5' => array(
          'module' => 'views',
          'delta' => 'syclops_session-block_5',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('User');
  $export['project-admin-session'] = $context;

  return $export;
}
