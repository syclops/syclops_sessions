<?php
/**
 * @file
 * syclops_sessions.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function syclops_sessions_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function syclops_sessions_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function syclops_sessions_node_info() {
  $items = array(
    'session' => array(
      'name' => t('Session'),
      'base' => 'node_content',
      'description' => t('Holds session data for specific connection to a remote server machine.'),
      'has_title' => '1',
      'title_label' => t('Session Name'),
      'help' => '',
    ),
  );
  return $items;
}
